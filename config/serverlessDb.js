const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let isConnected;

module.exports = connectToDatabase = () => {
  const promise = new Promise((resolve, reject) => {
    console.log(process.env.MONGO_URI);
    if (isConnected) {
      console.log("=> using existing database connection");
      return Promise.resolve();
    }

    console.log("=> using new database connection");
    return mongoose
      .connect(process.env.MONGO_URI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      })
      .then((db) => {
        console.log("call after connected", db.connections[0].readyState);
        isConnected = db.connections[0].readyState;
        resolve();
      });
  });
  return promise;
};
